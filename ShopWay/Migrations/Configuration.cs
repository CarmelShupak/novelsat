using System.Collections.Generic;
using ShopWay.Entities;

namespace ShopWay.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ShopWay.Entities.ShopWayDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(ShopWay.Entities.ShopWayDbContext context)
        {
            var products = new List<Product>
            {
                new Product {ProductName = "���� �����", ProductPrice = 14.90, ProductImage = "/App/Images/1.png"},
                new Product {ProductName = "���� ����", ProductPrice = 11.90, ProductImage = "/App/Images/4.png"},
                new Product {ProductName = "���� �����", ProductPrice = 16.30, ProductImage = "/App/Images/5.png"},
                new Product {ProductName = "����� ���", ProductPrice = 11.60, ProductImage = "/App/Images/1.png"},
                new Product {ProductName = "����� �����", ProductPrice = 16.70, ProductImage = "/App/Images/4.png"},
                new Product {ProductName = "����� ������", ProductPrice = 9.90, ProductImage = "/App/Images/5.png"},
                new Product {ProductName = "����� ����", ProductPrice = 5.40, ProductImage = "/App/Images/1.png"},
                new Product {ProductName = "��� ��� ����", ProductPrice = 11.90, ProductImage = "/App/Images/4.png"},
                new Product {ProductName = "��� ����", ProductPrice = 21.90, ProductImage = "/App/Images/5.png"},
                new Product {ProductName = "��� ����", ProductPrice = 17.00, ProductImage = "/App/Images/5.png"},
                new Product {ProductName = "��� ������", ProductPrice = 11.30, ProductImage = "/App/Images/5.png"}
            };
            products.ForEach(c=>context.products.AddOrUpdate(d=>d.ProductName,c));
        }
    }
}
