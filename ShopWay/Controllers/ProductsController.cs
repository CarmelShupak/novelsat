﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ShopWay.Entities;

namespace ShopWay.Controllers
{
    public class ProductsController : ApiController
    {
        ShopWayDbContext _context = new ShopWayDbContext();

       [HttpGet]
       public List<Product> GetProducts()
       {
           return _context.products.ToList();
       }

       //[HttpGet]
       //public List<Product> GetCartProducts()
       //{
       //    return null;
       //} 
    }
}
