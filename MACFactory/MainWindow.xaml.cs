﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MACFactory.Entities;
using System.Text.RegularExpressions;

namespace MACFactory
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MACFactoryDbContext _context = new MACFactoryDbContext();
        private List<MAC_Allocation> _newMacAllocations;

        public MainWindow()
        {
            InitializeComponent();
            FillTypeComboBox();
            ViewProductsAndMacAllocations();
        }
        private void ViewProductsAndMacAllocations()
        {
            FillProductsDataGrid();
            FillNewMacAllocationsList();
        }

        private void btnProduce_Click(object sender, RoutedEventArgs e)
        {
            int a;
            if (int.TryParse(tbQuantity.Text.Trim(), out a) && cbType.SelectedItem != null)
            {
                _newMacAllocations = CreateNewMacAllocations(int.Parse(tbQuantity.Text),
                    (cbType.SelectedItem as ProductType).Type);
                _context.MAC_Allocations.AddRange(_newMacAllocations);
                _context.SaveChanges();
                ViewProductsAndMacAllocations();
            }
            else
                MessageBox.Show("Quantity must be numeric and you must select a product type!");
        }

        private string ConvertDecimalToHex(long dec)
        {
            var hex = dec.ToString("X");            
            return hex;
        }
        private long ConvertHexToDecimal(string hex)
        {
            hex = Regex.Replace(hex, @"[:]", "");
            return long.Parse(hex, System.Globalization.NumberStyles.HexNumber);
        }
        private List<MAC_Allocation> CreateNewMacAllocations(int quantity, string type)
        {
            var maList = new List<MAC_Allocation>();
            var prevAddress = GetLastMacAddress();
            for (int i = 0; i < quantity; i++)
            {
                var newAddress = GetNextMacAddress(prevAddress);
                maList.Add(new MAC_Allocation
                {
                    ProductType = type,
                    MacAddress = newAddress
                });
                prevAddress = ConvertHexToDecimal(newAddress);
            }
            return maList;
        }
        private long GetLastMacAddress()
        {
            var macAdresses = _context.MAC_Allocations.ToList();
            return macAdresses.Max(c => ConvertHexToDecimal(Regex.Replace(c.MacAddress, @"[:]", "")));
        }
        private string GetNextMacAddress(long address)
        {

            var newAddress =  ConvertDecimalToHex(++address);
            while (newAddress.Length < 12)
                newAddress = "0" + newAddress;
            newAddress = Regex.Replace(newAddress, ".{2}", "$0:");
            newAddress = newAddress.Substring(0, newAddress.Length - 1);
            return newAddress;
        }
        private void FillProductsDataGrid()
        {
            ProductsDataGrid.DataContext = _context.MAC_Allocations.ToList(); ;
        }
        private void FillTypeComboBox()
        {
            cbType.DataContext = _context.ProductTypes.ToList();
        }

        private void FillNewMacAllocationsList()
        {
            NewMacAllocationsListView.DataContext = _newMacAllocations;
        }

        private void tbQuantity_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                btnProduce_Click(null,null);
        }

        private void NewMacAllocationsListView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var animation = (Storyboard) FindResource("NewMacAllocationsStoryboard");
            animation.Begin(NewMacAllocationsListView);
        }
    }
}
