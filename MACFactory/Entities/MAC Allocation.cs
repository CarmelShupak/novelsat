﻿namespace MACFactory.Entities
{
    class MAC_Allocation
    {
        public int Id { get; set; }
        public string ProductType { get; set; }
        public string MacAddress { get; set; }
    }
}
    