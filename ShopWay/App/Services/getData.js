﻿myApp.factory('getData', function($resource) {
    return $resource('/api/Products/:action', {action: '@action'});
})