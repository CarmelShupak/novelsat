﻿myApp.controller('searchController', function ($scope, $state, getData, myCart) {
    $scope.openList = function() {
        $state.transitionTo('home.search.list');
    }

    $scope.products = getData.query({ action: 'GetProducts' });

    $scope.addToCart = function (product) {
        myCart.products.push(product);
    }

    $scope.getActiveItem = function (item) {
        return $scope.activeItem == item;
    }

    $scope.setActiveItem = function (item) {
        $scope.activeItem = item;
    }
        
});