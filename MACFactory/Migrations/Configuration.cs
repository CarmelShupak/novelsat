using System.Collections.Generic;
using System.Windows.Documents;
using MACFactory.Entities;

namespace MACFactory.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MACFactory.Entities.MACFactoryDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(MACFactory.Entities.MACFactoryDbContext context)
        {
            var product = new MAC_Allocation
            {
                ProductType = "X",
                MacAddress = "00:EC:11:00:00:F0"
            };


            var productTypes = new List<ProductType>
            {
                new ProductType {Type = "X"},
                new ProductType {Type = "Y"},
                new ProductType {Type = "Z"}
            };

            context.MAC_Allocations.AddOrUpdate(c=>c.MacAddress,product);
            productTypes.ForEach(c => context.ProductTypes.AddOrUpdate(d => d.Type, c));
            context.SaveChanges();
        }
    }
}
