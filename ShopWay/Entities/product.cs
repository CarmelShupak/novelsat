﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopWay.Entities
{
    public class Product
    {
        public int id { get; set; }
        //public string ProductCategory { get; set; }
        public string ProductName { get; set; }
        public double ProductPrice { get; set; }
        public string ProductImage { get; set; }
        //public double ProductWeight { get; set; }
        //public Boolean ProductOnSale { get; set; }
    }
}