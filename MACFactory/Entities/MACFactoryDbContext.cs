﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MACFactory.Entities
{
    class MACFactoryDbContext : DbContext
    {
        public DbSet<MAC_Allocation> MAC_Allocations { get; set; }
        public DbSet<ProductType> ProductTypes { get; set; }
    }
}
