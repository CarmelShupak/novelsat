﻿var myApp = angular.module('myApp', ['ui.router', 'ngResource']).config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/home');

    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: 'App/Views/home.html',
            controller: 'homeController'
        })
        .state('home.cart', {
            url: '/cart',
            templateUrl: 'App/Templates/cart.html',
            controller: 'cartController'
        })
        .state('home.shareCart', {
            url: '/shareCart',
            templateUrl: 'App/Templates/shareCart.html',
            controller: 'shareCartController'
        })
        .state('home.map', {
            url: '/map',
            templateUrl: 'App/Templates/map.html',
            controller: 'mapController'
        })
        .state('home.sales', {
            url: '/sales',
            templateUrl: 'App/Templates/sales.html',
            controller: 'salesController'
        })
        .state('home.search', {
            url: '/search',
            templateUrl: 'App/Templates/search.html',
            controller: 'searchController'
        })
        .state('home.search.list', {
            templateUrl: 'App/Templates/searchList.html',
            controller: 'searchController'
        });

});