﻿myApp.controller('homeController', function($scope, $location) {
    $scope.isActive = function (path) {
        return $location.path() === path;
    }
});